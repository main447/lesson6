
public class Task6 {
    public static void main(String[] args) {
        Product apple = new Product("apple",50,10);
        Product hat = new Product("hat",500,3);
        Category food = new Category("food",apple);
        Category clothes = new Category("clothes",hat);
        Basket basket = new Basket(hat);
        User admin = new User("admin",1234,basket);
    }
}
