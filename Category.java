import java.util.ArrayList;
import java.util.List;

public class Category {
    public String name;
    public List<Object> products = new ArrayList<>();

    public Category(String n,Object prod){
        name=n;
        products.add(prod);
    }
}
